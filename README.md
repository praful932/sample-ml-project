# sample-ml-project

### Project Setup
- [Install poetry](https://python-poetry.org/docs/#installation)
- Install requirements - `poetry install`
- Activate env
    - Windows - ` source "$( poetry env list --full-path | grep Activated | cut -d' ' -f1 )/Scripts/activate" `
    - Linux - ` source "$( poetry env list --full-path | grep Activated | cut -d' ' -f1 )/bin/activate" `


### Handy Commands
- `poetry export -f requirements.txt --output requirements.txt --without-hashes`